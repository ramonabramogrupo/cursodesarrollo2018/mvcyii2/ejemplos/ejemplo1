<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias".
 *
 * @property int $id
 * @property string $titulo
 * @property string $texto
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'], 'required'],
            [['texto'], 'string'],
            [['titulo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo de la Noticia',
            'texto' => 'Texto de la Noticia',
        ];
    }
    
    public static function mostrarUna(){
        /**
         * numero total de registros
         */
        $total=self::find()->count();
        /**
         * Calculo una posicion aleatoria
         */
        $n=rand(0, $total-1);
        
        return self::find()->offset($n)->limit(1)->one();
             
    }
}
